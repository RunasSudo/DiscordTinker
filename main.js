// ==UserScript==
// @name        DiscordTinker
// @namespace   https://yingtongli.me
// @include     https://discordapp.com/channels/*
// @version     12
// @grant       none
// @run-at      document-start
// ==/UserScript==

/*
    DiscordTinker
    Copyright © 2017-18  RunasSudo (Yingtong Li)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

if (typeof(GM_info) === 'undefined') {
	// Dummy for testing purposes
	GM_info = {script: {}};
}

(function(DiscordTinker) {
	DiscordTinker.USER_AGENT = 'DiscordTinker (https://runassudo.github.io, ' + GM_info.script.version + ')';
	
	console.log('DiscordTinker ' + GM_info.script.version + ' loaded!');
	
	// Discord now unsets window.localStorage for security. We will restore it so we can access it from the script.
	// WARNING: This opens the user up to potential attacks on the API token. Take care!
	if (!DiscordTinker.localStorage) {
		DiscordTinker.localStorage = window.localStorage;
	}
	
	DiscordTinker.token = JSON.parse(DiscordTinker.localStorage.getItem('token'));
	
	// Miscellaneous utility functions
	DiscordTinker.Util = {};
	DiscordTinker.Util.onLoad = function(callback) {
		if (document.readyState === 'complete') {
			callback();
		} else {
			window.addEventListener('load', callback);
		}
	};
	
	// Patching helper functions
	DiscordTinker.Util.patch = function(obj, attr, func) {
		// Override existing patch if present
		if (obj[attr].__discord_tinker_patched) {
			func.__discord_tinker_patched = obj[attr].__discord_tinker_patched;
		} else {
			func.__discord_tinker_patched = obj[attr];
		}
		// Copy prototype for constructors
		if (obj[attr].prototype) {
			func.prototype = obj[attr].prototype;
		}
		// Perform the patch
		obj[attr] = func;
	}
	DiscordTinker.Util.patchAfter = function(obj, attr, funcAfter) {
		var patchedFunction = function() {
			var result = patchedFunction.__discord_tinker_patched.apply(this, arguments);
			return funcAfter(result);
		};
		DiscordTinker.Util.patch(obj, attr, patchedFunction);
	}
	DiscordTinker.Util.patchBefore = function(obj, attr, funcBefore) {
		var patchedFunction = function() {
			var args = funcBefore.apply(this, arguments);
			var result = patchedFunction.__discord_tinker_patched.apply(this, args);
			return result;
		};
		DiscordTinker.Util.patch(obj, attr, patchedFunction);
	}
	
	// DiscordTinker Plugin API
	DiscordTinker.Plugin = {};
	DiscordTinker.Plugin.listeners = {};
	DiscordTinker.Plugin.addListener = function(eventType, listener) {
		if (DiscordTinker.Plugin.listeners[eventType] === undefined) {
			DiscordTinker.Plugin.listeners[eventType] = [];
		}
		DiscordTinker.Plugin.listeners[eventType].push(listener);
	};
	DiscordTinker.Plugin.removeListener = function(eventType, listener) {
		if (DiscordTinker.Plugin.listeners[eventType] !== undefined) {
			if (listener === undefined) {
				// Remove all listeners. Avoid in production.
				console.warn('Removing all listeners for ' + eventType);
				DiscordTinker.Plugin.listeners[eventType].length = 0;
			} else {
				var index = DiscordTinker.Plugin.listeners.indexOf(listener);
				if (index >= 0) {
					DiscordTinker.Plugin.listeners.splice(index, 1);
				} else {
					throw 'Attempted to remove non-existent listener';
				}
			}
		}
	};
	DiscordTinker.Plugin.fireEvent = function(eventType, event) {
		if (DiscordTinker.Plugin.listeners[eventType] !== undefined) {
			for (var listener of DiscordTinker.Plugin.listeners[eventType]) {
				listener(event);
			}
		}
	};
	
	// HTTP API handling
	DiscordTinker.HTTP = {};
	DiscordTinker.HTTP.xhr = function(method, url, callback, headers, payload) {
		var xhr = new XMLHttpRequest();
		
		xhr.open(method, url);
		
		xhr.setRequestHeader('Authorization', DiscordTinker.token);
		xhr.setRequestHeader('User-Agent', DiscordTinker.USER_AGENT);
		if (headers) {
			for (header in headers) {
				xhr.setRequestHeader(header, headers[header]);
			}
		}
		
		xhr.addEventListener('load', function() {
			callback(this);
		});
		
		if (payload) {
			xhr.send(payload);
		} else {
			xhr.send();
		}
	};
	
	// WebSocket low-level handling
	DiscordTinker.WebSocket = {};
	DiscordTinker.WebSocket.VERSION = 6;
	
	DiscordTinker.WebSocket.ws = null;
	DiscordTinker.WebSocket.shouldRetry = true;
	DiscordTinker.WebSocket.wsIsReady = false;
	DiscordTinker.WebSocket.wsUrl = 'wss://gateway.discord.gg';
	DiscordTinker.WebSocket.connect = function() {
		DiscordTinker.WebSocket.ws = new WebSocket(DiscordTinker.WebSocket.wsUrl + '?v=' + DiscordTinker.WebSocket.VERSION + '&encoding=json');
		DiscordTinker.WebSocket.ws.onmessage = DiscordTinker.WebSocket.onmessage;
		DiscordTinker.WebSocket.ws.onerror = DiscordTinker.WebSocket.onerror;
		DiscordTinker.WebSocket.ws.onclose = DiscordTinker.WebSocket.onclose;
	};
	DiscordTinker.WebSocket.connectAfterFailure = function() {
		DiscordTinker.HTTP.xhr('GET', 'https://discordapp.com/api/gateway', function(xhr) {
			var gatewayUrl = JSON.parse(xhr.responseText).url;
			if (gatewayUrl === DiscordTinker.WebSocket.wsUrl) {
				// Something's gone badly wrong
				// TODO: Exponential backoff
				console.log('DiscordTinker WebSocket URL hasn\'t changed. Retrying in 5 seconds.');
				setTimeout(DiscordTinker.WebSocket.connectAfterFailure, 5000);
			} else {
				console.log('DiscordTinker got new WebSocket URL: ' + gatewayUrl);
				DiscordTinker.WebSocket.wsUrl = gatewayUrl;
				DiscordTinker.WebSocket.connect();
			}
		});
	};
	
	DiscordTinker.WebSocket.onmessage = function(event) {
		var msg = JSON.parse(event.data);
		//console.log(msg);
		DiscordTinker.Gateway.onmessage(msg.op, msg.d, msg.s, msg.t);
	};
	DiscordTinker.WebSocket.onerror = function(event) {
		console.error('DiscordTinker WebSocket error', event);
		
		if (DiscordTinker.WebSocket.shouldRetry) {
			if (DiscordTinker.WebSocket.wsIsReady) {
				// Connection dropped, etc. Retry.
				DiscordTinker.WebSocket.connect();
			} else {
				// Incorrect websocket URL. Refresh.
				DiscordTinker.WebSocket.connectAfterFailure();
			}
		}
		
		DiscordTinker.WebSocket.wsIsReady = false;
		DiscordTinker.Gateway.onclose();
	};
	DiscordTinker.WebSocket.onclose = function(event) {
		console.log('DiscordTinker WebSocket closed', event);
		DiscordTinker.WebSocket.wsIsReady = false;
		DiscordTinker.Gateway.onclose();
		
		// Try to reconnect
		if (DiscordTinker.WebSocket.shouldRetry) {
			setTimeout(DiscordTinker.WebSocket.connect, 5000);
		}
	};
	
	// High-level gateway API operations
	DiscordTinker.Gateway = {};
	DiscordTinker.Gateway.Op = {
		DISPATCH: 0,
		HEARTBEAT: 1,
		IDENTIFY: 2,
		STATUS_UPDATE: 3,
		VOICE_STATE_UPDATE: 4,
		VOICE_SERVER_PING: 5,
		RESUME: 6,
		RECONNECT: 7,
		REQUEST_GUILD_MEMBERS: 8,
		INVALID_SESSION: 9,
		HELLO: 10,
		HEARTBEAT_ACK: 11
	};
	
	DiscordTinker.Gateway.send = function(op, d, s, t) {
		msg = {op: op, d: d};
		if (s) {
			msg.s = s;
		}
		if (t) {
			msg.t = t;
		}
		//console.log('DiscordTinker Gateway sending', msg);
		DiscordTinker.WebSocket.ws.send(JSON.stringify(msg));
	};
	DiscordTinker.Gateway.onmessage = function(op, d, s, t) {
		switch (op) {
			case DiscordTinker.Gateway.Op.HELLO:
				console.log('DiscordTinker Gateway got Hello');
				
				// Begin sending heartbeats
				DiscordTinker.Gateway.heartbeatTimer = setInterval(DiscordTinker.Gateway.heartbeat, d.heartbeat_interval);
				
				// Send Identify
				DiscordTinker.Gateway.identify();
				break;
			case DiscordTinker.Gateway.Op.DISPATCH:
				DiscordTinker.Gateway.lastDispatchS = s;
				switch (t) {
					case 'READY':
						console.log('DiscordTinker WebSocket ready');
						
						// Check version
						if (d.v !== DiscordTinker.WebSocket.VERSION) {
							console.error('DiscordTinker Gateway got unsupported version ' + d.v);
							window.alert('DiscordTinker Gateway got unsupported version ' + d.v);
							DiscordTinker.WebSocket.shouldRetry = false;
							DiscordTinker.WebSocket.ws.close();
							return;
						}
						
						console.log('DiscordTinker Gateway got version ' + d.v);
						DiscordTinker.WebSocket.wsIsReady = true;
						
						DiscordTinker.Gateway.heartbeat();
				}
				break;
			case DiscordTinker.Gateway.Op.INVALID_SESSION:
				// Identify was rate limited
				console.error('DiscordTinker Gateway got Invalid Session. Retrying after 1 second');
				setTimeout(DiscordTinker.Gateway.identify, 1000);
				break;
			case DiscordTinker.Gateway.Op.HEARTBEAT_ACK:
				console.log('DiscordTinker got HEARTBEAT_ACK');
				break;
		}
	};
	DiscordTinker.Gateway.onclose = function() {
		clearInterval(DiscordTinker.Gateway.heartbeatTimer);
	};
	
	DiscordTinker.Gateway.identify = function() {
		DiscordTinker.Gateway.send(DiscordTinker.Gateway.Op.IDENTIFY, {
			token: DiscordTinker.token,
			properties: {
				'$os': 'linux',
				'$browser': 'DiscordTinker',
				'$device': 'DiscordTinker',
				'$referrer': '',
				'$referring_domain': ''
			},
			compress: false,
			large_threshold: 50,
			shard: [0, 1] // TODO: Remove
		});
	};
	
	DiscordTinker.Gateway.heartbeatTimer = null;
	DiscordTinker.Gateway.lastDispatchS = null;
	DiscordTinker.Gateway.heartbeat = function() {
		DiscordTinker.Gateway.send(DiscordTinker.Gateway.Op.HEARTBEAT, DiscordTinker.Gateway.lastDispatchS);
		console.log('DiscordTinker sent HEARTBEAT');
		DiscordTinker.Plugin.fireEvent('heartbeat');
	};
	
	// https://support.discordapp.com/hc/en-us/articles/115002192352-Automated-user-accounts-self-bots-
	//DiscordTinker.WebSocket.connect();
	
	// Internal stuff
	DiscordTinker.Int = {};
	DiscordTinker.Int.WebpackModules = {};
	DiscordTinker.Int.WebpackModules.find = function(filter) {
		for (var i in DiscordTinker.Int.WebpackModules.require.c) {
			// Ignore inherited properties
			if (DiscordTinker.Int.WebpackModules.require.c.hasOwnProperty(i)) {
				var module = DiscordTinker.Int.WebpackModules.require.c[i].exports;
				if (module && module.__esModule && module.default) {
					module = module.default;
				}
				//console.log(module);
				if (module && filter(module)) {
					return module;
				}
			}
		}
		return null;
	};
	DiscordTinker.Int.WebpackModules.findByProperties = function(properties) {
		return DiscordTinker.Int.WebpackModules.find(function(module) {
			// Skip if primitive
			if (module !== Object(module)) {
				return false;
			}
			//console.log(Object.keys(module));
			for (var property of properties) {
				if (!(property in module)) {
					return false;
				}
			}
			return true;
		});
	};
	
	// We must wait for the Javascript to be loaded before patching
	DiscordTinker.Util.onLoad(function() {
		if (typeof(webpackJsonp) === 'function') {
			// Now unused?
			DiscordTinker.Int.WebpackModules.require = webpackJsonp([], {'__discord_tinker__': function(module, exports, req) { exports.default = req; }}, ['__discord_tinker__']).default;
		} else {
			// Webpack 4
			DiscordTinker.Int.WebpackModules.require = webpackJsonp.push([[], {'__discord_tinker__': function(module, exports, req) { exports.default = req; }}, [['__discord_tinker__']]]).default;
		}
		delete DiscordTinker.Int.WebpackModules.require.m['__discord_tinker__'];
		delete DiscordTinker.Int.WebpackModules.require.c['__discord_tinker__'];
		
		DiscordTinker.Int.React = DiscordTinker.Int.WebpackModules.findByProperties(['Component', 'PureComponent', 'Children', 'createElement', 'cloneElement']);
		
		DiscordTinker.Util.patchBefore(DiscordTinker.Int.React, 'createElement', function() {
			if (arguments[0].displayName) {
				if (DiscordTinker.Int.ReactComponents.components[arguments[0].displayName] !== arguments[0]) {
					DiscordTinker.Int.ReactComponents.components[arguments[0].displayName] = arguments[0];
					DiscordTinker.Plugin.fireEvent('reactNewComponent', arguments[0]);
				}
			}
			DiscordTinker.Plugin.fireEvent('reactCreateElement', arguments);
			return arguments;
		});
	});
	
	DiscordTinker.Int.ReactComponents = {};
	DiscordTinker.Int.ReactComponents.components = {};
	DiscordTinker.Int.ReactComponents.createFunnyElement = function(type, props, key, children) {
		// a la r(type, props, key, children...)
		props.children = children;
		return {
			$$typeof: Symbol.for('react.element'),
			type: type,
			key: key === undefined ? null : '' + key,
			ref: null,
			props: props,
			_owner: null
		};
	}
	DiscordTinker.Int.ReactComponents.getComponent = function(displayName, callback) {
		if (DiscordTinker.Int.ReactComponents.components[displayName] !== undefined) {
			// Component already loaded. Callback immediately.
			callback(DiscordTinker.Int.ReactComponents.components[displayName]);
		} else {
			// Component not yet loaded. Register an event to callback when loaded.
			var listener = function(component) {
				if (component.displayName === displayName) {
					callback(component);
					DiscordTinker.Plugin.removeListener('reactNewComponent', listener);
				}
			};
			DiscordTinker.Plugin.addListener('reactNewComponent', listener);
		}
	};
	DiscordTinker.Int.ReactComponents.patchRender = function(displayName, callback) {
		// Every time we create an element, we need to patch the render function
		DiscordTinker.Plugin.addListener('reactCreateElement', function(event) {
			if (event[0].displayName === displayName) {
				DiscordTinker.Util.patch(event[0].prototype, 'render', callback(event, event[0].prototype.render));
			}
		});
	};
	
	// Behaviour stuff
	DiscordTinker.Prefs = {};
	DiscordTinker.Prefs.data = {};
	if (DiscordTinker.localStorage.getItem('discord_tinker_prefs')) {
		DiscordTinker.Prefs.data = JSON.parse(DiscordTinker.localStorage.getItem('discord_tinker_prefs'));
	}
	DiscordTinker.Prefs.getPref = function(key, def) {
		return DiscordTinker.Prefs.data[key] === undefined ? def : DiscordTinker.Prefs.data[key];
	};
	DiscordTinker.Prefs.setPref = function(key, val) {
		DiscordTinker.Prefs.data[key] = val;
		DiscordTinker.Prefs.save();
	};
	DiscordTinker.Prefs.save = function() {
		DiscordTinker.localStorage.setItem('discord_tinker_prefs', JSON.stringify(DiscordTinker.Prefs.data));
	};
	
	DiscordTinker.Chat = {};
	DiscordTinker.Chat.getChannelIds = function() {
		var bits = window.location.pathname.split('/');
		return [bits[2], bits[3]];
	}
	
	DiscordTinker.UI = {};
	DiscordTinker.UI.commands = {};
	window.addEventListener('keypress', function(evt) {
		if (evt.key === 'q' && evt.altKey) {
			// Commands!
			var command = prompt('DiscordTinker command:');
			if (command === null) {
				return;
			}
			var commandBits = command.split(' ');
			if (commandBits.length == 0) {
				return;
			}
			
			if (DiscordTinker.UI.commands[commandBits[0]] !== undefined) {
				DiscordTinker.UI.commands[commandBits[0]](command, commandBits);
			} else {
				alert('Unknown command');
			}
		}
	});
	
	DiscordTinker.UI.popoutButtons = [];
	DiscordTinker.Int.ReactComponents.patchRender('Popouts', function(event, orig) {
		return function() {
			var popouts = orig.apply(this, arguments);
			if (popouts.props.children[1].length > 0) {
				var popout = popouts.props.children[1][0];
				DiscordTinker.Util.patchAfter(popout.props, 'render', function(optionPopout) {
					//if (optionPopout.type.displayName === 'OptionPopout') {
					if (optionPopout.props.hasOwnProperty('canDelete')) {
						DiscordTinker.Util.patchAfter(optionPopout.type.prototype, 'render', function(optionPopoutElement) {
							// Find a button to use as a template
							function findButtonRecursively(element) {
								for (var child of element.props.children) {
									if (child && child.props) {
										if (typeof(child.props.children) === 'string') {
											if (child.props.role === 'menuitem') {
												return child;
											}
										} else {
											// Recurse
											var ret = findButtonRecursively(child);
											if (ret) {
												return ret;
											}
										}
									}
								}
								return null;
							}
							
							var template = findButtonRecursively(optionPopoutElement);
							
							if (template) {
								for (var button of DiscordTinker.UI.popoutButtons) {
									// Add the button
									(function(button) {
										var button_obj = Object.assign({}, template);
										button_obj.props = Object.assign({}, template.props);
										button_obj.props.children = button.label;
										button_obj.props.onClick = function() {
											button.onClick(optionPopout);
										};
										optionPopoutElement.props.children.push(button_obj);
										//optionPopoutElement.props.children.push(DiscordTinker.Int.ReactComponents.createFunnyElement('div', { className: 'btn-item', onClick: function() {
										//	button.onClick(optionPopout);
										//} }, undefined, [button.label]));
									})(button);
								}
							} else {
								console.log('Could not find a suitable button to clone');
							}
							return optionPopoutElement;
						});
					}
					return optionPopout;
				});
			}
			return popouts;
		}
	});
})(window.DiscordTinker = window.DiscordTinker || {});
